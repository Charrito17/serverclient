
import java.io.*;
import java.net.*;

class MyServer0 {
	public static void main(String args[]) throws Exception {

		ServerSocket ss = new ServerSocket(8888);
		Socket s = ss.accept();
		DataInputStream din = new DataInputStream(s.getInputStream());
		DataOutputStream dout = new DataOutputStream(s.getOutputStream());
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str1 = "", str2 = "";

		while (!str1.contains("*")) {

			str1 = din.readUTF();

			System.out.println(str1);
			while (!str2.contains("Finished") && !str1.contains("*")) {
				str2 += br.readLine();
			}

			dout.writeUTF("Server:\t" + str2);
			dout.flush();
			str2 = "";

		}

		din.close();
		s.close();
		ss.close();

	}
}
